use std::str::FromStr;
use reqwest::header::{HeaderValue, HeaderName};
use graphql_client::{reqwest::post_graphql_blocking, GraphQLQuery};
use reqwest::blocking::{Client};
use crate::show_repository::ShowdownStatus::{Active, Completed, Pending};
use chrono::{Utc};

const DATABASE_URL: &str = "https://66064766-2312-4e54-9480-e0de5d05b105-us-east-1.apps.astra.datastax.com/api/graphql/showdown";


#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct GetShow;

#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct GetShows;

#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct CreateShow;

#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct CreateCharacters;

#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct CreateShowdown;

#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct GetCharacters;

#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct GetShowdowns;

#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct CreateEliminationVote;

#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct CreateEliminationRound;


#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct GetEliminationVotes;

#[derive(GraphQLQuery)]
#[graphql(
schema_path = "schema.graphql",
query_path = "src/showquery.graphql",
response_derives = "Debug",
)]
struct GetEliminationRounds;

type Timestamp = i64;


#[derive(Debug, Default, Clone)]
pub struct Show {
    pub name: String,
    pub release_year: Option<String>,
    pub overview: Option<String>,
    pub tmdb_id: i64,
    pub characters: Vec<Character>,
    pub seasons: Vec<String>,
}

#[derive(Debug, Default, Clone)]
pub struct Character {
    pub character_name: String,
    pub actor_name: String,
    pub order: i64,
    pub credit_id: String,
    pub profile_path: String,
}

impl Character {
    pub fn from_tmdb_credit(credit: &crate::tmdb_repository::TmdbCredit) -> Character {
        let (name, credit_id) = if let (Some(name), Some(credit_id)) = (&credit.character, &credit.credit_id) { (name, credit_id) }
        else if let Some(roles) =&credit.roles { let role = roles.first().expect("Empty vector of roles");(&role.character,&role.credit_id)}
        else {panic!("Unable to get any data for character")};
        Character {
            character_name: name.to_string(),
            actor_name: credit.name.to_string(),
            credit_id: credit_id.to_string(),
            order: credit.order.clone(),
            profile_path: credit.profile_path.as_ref().unwrap_or(&"".to_string()).to_string(),
        }
    }

    fn from_get_show(response_data: &get_show::ResponseData) -> Vec<Character> {
        let response_characters = response_data.character
            .as_ref().map(|s| s.values.as_ref()).flatten();
        return match response_characters {
            None => vec![],
            Some(raw_data) => raw_data.iter().map(|r| Character {
                character_name: r.character_name.as_ref().unwrap().to_string(),
                actor_name: r.actor_name.as_ref().unwrap().to_string(),
                credit_id: r.credit_id.as_ref().unwrap().to_string(),
                order: r.order.as_ref().unwrap().clone(),
                profile_path: r.profile_path.as_ref().unwrap().to_string()
            }).collect()
        };
    }

    fn from_get_characters(response_data: &get_characters::ResponseData) -> Vec<Character> {
        let response_characters = response_data.character
            .as_ref().map(|s| s.values.as_ref()).flatten();
        return match response_characters {
            None => vec![],
            Some(raw_data) => raw_data.iter().map(|r| Character {
                character_name: r.character_name.as_ref().unwrap().to_string(),
                actor_name: r.actor_name.as_ref().unwrap().to_string(),
                credit_id: r.credit_id.as_ref().unwrap().to_string(),
                order: r.order.as_ref().unwrap().clone(),
                profile_path: r.profile_path.as_ref().unwrap().to_string()
            }).collect()
        };
    }
}

#[derive(Debug, Default, Clone)]
pub struct Showdown {
    pub id: String,
    pub show_id: i64,
    pub season: String,
    pub episode: String,
    pub credit_ids: Vec<String>,
    pub current_round: i64,
    pub type_: String,
    pub title: String,
    pub status: ShowdownStatus,
}

impl Showdown {
    pub fn is_startable(&self) -> bool {
        match self.status {
            Pending => true,
            _ => false
        }
    }

    pub fn is_completable(&self) -> Result<bool, &str> {
        match self.type_.as_str() {
            "ELIMINATION" => Ok(i64::try_from(self.credit_ids.len()).unwrap() - self.current_round == 1),
            _ => Err("Unknown showdown type")
        }
    }

    pub fn is_active(&self) -> bool {
        match self.status {
            Active => true,
            _ => false
        }
    }
}

#[derive(Debug, Clone)]
pub enum ShowdownStatus {
    Pending,
    Active,
    Completed,
}

impl FromStr for ShowdownStatus {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Pending" => Ok(Pending),
            "Active" => Ok(Active),
            "Completed" => Ok(Completed),
            _ => Err(())
        }
    }
}

impl ShowdownStatus {
    pub fn to_str(&self) -> Result<&str, ()> {
        match self {
            Pending => Ok("Pending"),
            Active => Ok("Active"),
            Completed => Ok("Completed")
        }
    }
}

impl Default for ShowdownStatus {
    fn default() -> Self {
        Pending
    }
}

#[derive(Debug, Clone)]
pub struct EliminationVote {
    pub showdown_id: String,
    pub round_number: i64,
    pub user_id: String,
    pub credit_id: String,
}

impl EliminationVote {
    fn from_values(data: &get_elimination_votes::GetEliminationVotesEliminationVoteValues) -> EliminationVote {
        EliminationVote {
            round_number: data.round_number.as_ref().unwrap().clone(),
            showdown_id: data.showdown_id.as_ref().unwrap().to_string(),
            credit_id: data.credit_id.as_ref().unwrap().to_string(),
            user_id: data.user_id.as_ref().unwrap().to_string(),
        }
    }
}

#[derive(Debug, Default, Clone)]
pub struct EliminationRound {
    pub showdown_id: String,
    pub round_number: i64,
    pub credit_id: String,
}

impl EliminationRound {
    fn from_values(data: &get_elimination_rounds::GetEliminationRoundsEliminationRoundValues) -> EliminationRound {
        EliminationRound {
            showdown_id: data.showdown_id.as_ref().unwrap().to_string(),
            round_number: data.round_number.as_ref().unwrap().clone(),
            credit_id: data.credit_id.as_ref().unwrap().to_string(),
        }
    }
}


fn get_client() -> Result<Client, reqwest::Error> {
    let token = std::env::var("ASTRA_TOKEN").unwrap();
    let headers = std::iter::once((HeaderName::from_static("x-cassandra-token"), HeaderValue::try_from(token).unwrap())).collect();
    Client::builder().default_headers(headers).build()
}

pub struct ShowRepository {
    client: Client,
}

impl ShowRepository {
    pub fn new() -> ShowRepository {
        ShowRepository {
            client: get_client().unwrap()
        }
    }

    pub fn create_show(&self, show: &Show) -> Result<(), reqwest::Error> {
        let show_input: create_show::showInput = create_show::showInput {
            name: Some(show.name.to_string()),
            overview: show.overview.as_ref().map(String::to_string),
            release_year: show.release_year.as_ref().map(String::to_string),
            seasons: Some(show.seasons.iter().map(|s| Some(s.to_string())).collect()),
            tmdb_id: Some(show.tmdb_id.clone()),
        };
        let characters = show.characters.iter().map(|c| create_show::characterInput {
            show_id: Some(show.tmdb_id.clone()),
            season: Some("".to_string()),
            episode: Some("".to_string()),
            order: Some(c.order.clone()),
            credit_id: Some(c.credit_id.to_string()),
            character_name: Some(c.character_name.to_string()),
            actor_name: Some(c.actor_name.to_string()),
            profile_path: Some(c.profile_path.to_string())
        }).collect();
        let variables = create_show::Variables {
            show: show_input,
            characters: Some(characters),
        };
        post_graphql_blocking::<CreateShow, _>(&self.client, DATABASE_URL, variables).unwrap();
        Ok(())
    }

    pub fn create_characters(&self, characters: &Vec<Character>, show_id: &i64, season: &String, episode: &String) -> Result<(), reqwest::Error> {
        let characters = characters.iter().map(|c| create_characters::characterInput {
            show_id: Some(show_id.clone()),
            season: Some(season.to_string()),
            episode: Some(episode.to_string()),
            order: Some(c.order.clone()),
            credit_id: Some(c.credit_id.to_string()),
            character_name: Some(c.character_name.to_string()),
            actor_name: Some(c.actor_name.to_string()),
            profile_path: Some(c.profile_path.to_string())
        }).collect();
        let variables = create_characters::Variables {
            characters: Some(characters)
        };
        post_graphql_blocking::<CreateCharacters, _>(&self.client, DATABASE_URL, variables).unwrap();
        Ok(())
    }

    pub fn get_show(&self, tmdb_id: &i64) -> Result<Show, reqwest::Error> {
        let variables = get_show::Variables {
            tmdb_id: *tmdb_id
        };
        let response_body = post_graphql_blocking::<GetShow, _>(&self.client, DATABASE_URL, variables).unwrap();
        let response_data: get_show::ResponseData = response_body.data.expect("response data missing");
        let mut show = parse_show(&response_data).expect("Unable to parse show");
        let characters = Character::from_get_show(&response_data);
        show.characters = characters;
        Ok(show)
    }

    pub fn get_shows(&self) -> Result<Vec<Show>, reqwest::Error> {
        let variables = get_shows::Variables {};
        let response_body = post_graphql_blocking::<GetShows, _>(&self.client, DATABASE_URL, variables).unwrap();
        let response_data: get_shows::ResponseData = response_body.data.expect("response data missing");
        Ok(parse_shows(&response_data))
    }

    pub fn create_showdown(&self, showdown: &Showdown) -> Result<(), reqwest::Error> {
        let variables = create_showdown::Variables {
            showdown: create_showdown::showdownInput {
                id: Some(showdown.id.clone()),
                credit_ids: Some(showdown.credit_ids.iter().map(|c| Some(c.clone())).collect()),
                current_round: Some(showdown.current_round.clone()),
                episode: Some(showdown.episode.clone()),
                season: Some(showdown.season.clone()),
                show_id: Some(showdown.show_id.clone()),
                type_: Some(showdown.type_.clone()),
                title: Some(showdown.title.clone()),
                status: Some(showdown.status.to_str().unwrap().to_string()),
            }
        };
        post_graphql_blocking::<CreateShowdown, _>(&self.client, DATABASE_URL, variables)?;
        Ok(())
    }

    pub fn get_characters(&self, showdown: &Showdown) -> Result<Vec<Character>, reqwest::Error> {
        let variables = get_characters::Variables {
            show_id: showdown.show_id.clone(),
            season: showdown.season.clone(),
            episode: showdown.episode.clone(),
        };
        let response_body = post_graphql_blocking::<GetCharacters, _>(&self.client, DATABASE_URL, variables)?;
        let response_data: get_characters::ResponseData = response_body.data.expect("get characters response missing");
        let characters = Character::from_get_characters(&response_data)
            .iter()
            .filter(|c| showdown.credit_ids.contains(&c.credit_id))
            .map(|c| c.clone())
            .collect()
            ;
        Ok(characters)
    }

    pub fn get_showdowns(&self) -> Result<Vec<Showdown>, reqwest::Error> {
        let variables = get_showdowns::Variables {};
        let response_body = post_graphql_blocking::<GetShowdowns, _>(&self.client, DATABASE_URL, variables).unwrap();
        let response_data: get_showdowns::ResponseData = response_body.data.expect("response data missing");
        let response_showdowns = response_data.showdown
            .as_ref().map(|s| s.values.as_ref()).flatten();
        let showdowns = match response_showdowns {
            None => vec![],
            Some(showdowns) => showdowns.iter()
                .map(|s| Showdown {
                    id: s.id.as_ref().unwrap().clone(),
                    show_id: s.show_id.unwrap().clone(),
                    season: s.season.as_ref().unwrap().clone(),
                    episode: s.episode.as_ref().unwrap().clone(),
                    credit_ids: s.credit_ids.as_ref().unwrap().iter().flatten().map(|s| s.clone()).collect(),
                    current_round: s.current_round.as_ref().unwrap().clone(),
                    type_: s.type_.as_ref().unwrap().clone(),
                    title: s.title.as_ref().unwrap().clone(),
                    status: ShowdownStatus::from_str(&s.status.as_ref().unwrap()).unwrap(),
                }).collect()
        };
        Ok(showdowns)
    }

    pub fn vote_for_elimination(&self, showdown_id: &String, round_number: &i64, credit_id: &String, user_id: &String) -> Result<(), reqwest::Error> {
        let variables = create_elimination_vote::Variables {
            elimination_vote: create_elimination_vote::elimination_voteInput {
                showdown_id: Some(showdown_id.clone()),
                round_number: Some(round_number.clone()),
                credit_id: Some(credit_id.clone()),
                user_id: Some(user_id.clone()),
                vote_time: Some(Utc::now().timestamp_millis()),
            }
        };
        post_graphql_blocking::<CreateEliminationVote, _>(&self.client, DATABASE_URL, variables)?;
        Ok(())
    }

    pub fn get_elimination_votes(&self, showdown_id: &String, round_number: Option<&i64>) -> Result<Vec<EliminationVote>, reqwest::Error> {
        let variables = get_elimination_votes::Variables {
            showdown_id: showdown_id.clone(),
            round_number: round_number.map(|r| r.clone()),
        };
        let response: get_elimination_votes::ResponseData = post_graphql_blocking::<GetEliminationVotes, _>(&self.client, DATABASE_URL, variables)?.data.unwrap();
        let response_votes = response.elimination_vote.as_ref()
            .map(|v| v.values.as_ref()).flatten();
        let votes = match response_votes {
            None => vec![],
            Some(votes) => votes.iter()
                .map(|v| EliminationVote::from_values(&v)).collect()
        };
        Ok(votes)
    }

    pub fn create_elimination_round(&self, showdown_id: &String, round_number: &i64, credit_id: &String) -> Result<(), reqwest::Error> {
        let variables = create_elimination_round::Variables {
            elimination_round: create_elimination_round::elimination_roundInput {
                showdown_id: Some(showdown_id.clone()),
                round_number: Some(round_number.clone()),
                credit_id: Some(credit_id.clone()),
                create_time: Some(Utc::now().timestamp_millis()),
            }
        };
        post_graphql_blocking::<CreateEliminationRound, _>(&self.client, DATABASE_URL, variables)?;
        Ok(())
    }

    pub fn get_elimination_rounds(&self, showdown_id: &String) -> Result<Vec<EliminationRound>, reqwest::Error> {
        let variables = get_elimination_rounds::Variables {
            showdown_id: showdown_id.clone()
        };
        let response = post_graphql_blocking::<GetEliminationRounds, _>(&self.client, DATABASE_URL, variables)?.data.unwrap();
        let response_rounds = response.elimination_round.as_ref()
            .map(|r| r.values.as_ref()).flatten();
        let rounds = match response_rounds {
            None => vec![],
            Some(rounds) => rounds.iter()
                .map(|r| EliminationRound::from_values(&r)).collect()
        };
        Ok(rounds)
    }
}


fn parse_show(response_data: &get_show::ResponseData) -> Option<Show> {
    response_data.show
        .as_ref().map(|s| s.values.as_ref()).flatten()
        .map(|v| &v[0])
        .map(|s| Show::from_show_value(&s))
}

fn parse_shows(response_data: &get_shows::ResponseData) -> Vec<Show> {
    let response_shows = response_data.show
        .as_ref().map(|s| s.values.as_ref()).flatten();
    match response_shows {
        None => vec![],
        Some(shows) => shows.iter()
            .map(|s| Show::from_shows_value(&s)).collect()
    }
}

impl Show {
    fn from_show_value(data: &get_show::GetShowShowValues) -> Show {
        Show {
            name: data.name.as_ref().unwrap_or(&"".to_string()).to_string(),
            release_year: data.release_year.as_ref().map(|s| s.to_string()),
            overview: data.overview.as_ref().map(|s| s.to_string()),
            tmdb_id: *data.tmdb_id.as_ref().unwrap_or(&0),
            seasons: data.seasons.as_ref().unwrap().iter().flatten().map(String::to_string).collect(),
            characters: vec![],
        }
    }

    fn from_shows_value(data: &get_shows::GetShowsShowValues) -> Show {
        Show {
            name: data.name.as_ref().unwrap_or(&"".to_string()).to_string(),
            release_year: data.release_year.as_ref().map(|s| s.to_string()),
            overview: data.overview.as_ref().map(|s| s.to_string()),
            tmdb_id: *data.tmdb_id.as_ref().unwrap_or(&0),
            seasons: vec![],
            characters: vec![],
        }
    }
}


