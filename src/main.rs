use std::io::{Cursor, Stdout, Write};
use std::path::Path;
pub use crossterm::{
    cursor,
    event::{self, Event, KeyCode, KeyEvent},
    execute, queue, style,
    terminal::{self, ClearType},
    Command, Result,
};
use crossterm::style::{Attribute, Attributes, Color};
use crossterm::terminal::ClearType::All;
use prettytable::*;
use crate::show_repository::{Character, Show, Showdown, ShowdownStatus, ShowRepository};
use crate::show_repository::ShowdownStatus::Pending;
use crate::tmdb_repository::{TmdbRepository, TmdbTvShow};
use crate::UserCommand::{CreateShowdown, Landing, Login, NewShow, Quit, SelectShow, ShowSelection, VoteOnRound};
use itertools::Itertools;
use viuer::Config;

mod show_repository;
mod tmdb_repository;

fn main() {
    do_interface();
}

fn do_interface() {
    let mut app = App::new();
    let mut error = None;
    app.setup_interface().expect("Error setting up interface");
    if let Err(e) = app.run() {
        error = Some(e);
    }
    app.teardown_interface().expect("Error tearing down interface");
    if error.is_some() {
        println!("Error running app {:?}", error.unwrap())
    }
}


#[derive(Debug, Clone)]
enum UserCommand {
    ShowSelection,
    SelectShow(Show),
    Quit,
    NewShow,
    CreateShowdown(Show),
    Landing,
    ShowdownSelection,
    VoteOnRound(Showdown),
    Login(Box<Option<UserCommand>>),
}

impl UserCommand {
    fn should_exit(&self) -> bool {
        match *self {
            Quit => true,
            _ => false
        }
    }
}

fn highlight_row(highlight_index: usize, default_index: Option<usize>, t: &mut Table) {
    for cell in t.get_mut_row(highlight_index + 1).unwrap().iter_mut() {
        cell.reset_style();
        cell.style(Attr::ForegroundColor(prettytable::color::CYAN));
        cell.style(Attr::Bold);
    }
    if default_index.is_some() {
        let default = default_index.unwrap();
        for cell in t.get_mut_row(default + 1).unwrap().iter_mut() {
            cell.reset_style();
        }
    }
}

struct App {
    tmdb_repository: TmdbRepository,
    show_repository: ShowRepository,
    stdout: Stdout,
    user_name: Option<String>,
}

impl App {
    fn new() -> App {
        App {
            tmdb_repository: TmdbRepository::new(),
            show_repository: ShowRepository::new(),
            stdout: std::io::stdout(),
            user_name: Option::None,
        }
    }

    fn run(&mut self) -> Result<()> {
        let mut next_command = Landing;
        loop {
            next_command = self.do_command(&next_command)?;
            if next_command.should_exit() {
                break;
            }
        }
        Ok(())
    }

    fn do_command(&mut self, command: &UserCommand) -> Result<UserCommand> {
        match command {
            UserCommand::ShowSelection => self.show_selection(),
            UserCommand::SelectShow(show) => self.select_show(&show),
            UserCommand::Quit => Ok(UserCommand::Quit),
            UserCommand::NewShow => self.new_show(),
            UserCommand::CreateShowdown(show) => self.create_showdown(show),
            UserCommand::Landing => self.landing(),
            UserCommand::ShowdownSelection => self.showdown_selection(),
            UserCommand::VoteOnRound(showdown) => self.vote_on_round(showdown),
            UserCommand::Login(possible_next) => self.login(possible_next)
        }
    }

    fn landing(&mut self) -> Result<UserCommand> {
        queue!(self.stdout,terminal::Clear(ClearType::All),cursor::MoveTo(0,0),style::Print("[1] Show selection"),cursor::MoveToNextLine(1))?;
        queue!(self.stdout,style::Print("[2] Showdown selection"),cursor::MoveToNextLine(1))?;
        queue!(self.stdout,style::Print("[3] Login"))?;
        self.stdout.flush()?;
        let next_command = loop {
            break match read_char()? {
                KeyCode::Char('1') => UserCommand::ShowSelection,
                KeyCode::Char('2') => UserCommand::ShowdownSelection,
                KeyCode::Char('3') => UserCommand::Login(Box::new(None)),
                KeyCode::Char('q') | KeyCode::Char('Q') | KeyCode::Esc => UserCommand::Quit,
                _ => {
                    continue;
                }
            };
        };
        Ok(next_command)
    }

    fn showdown_selection(&mut self) -> Result<UserCommand> {
        queue!(self.stdout,cursor::MoveTo(0,0),terminal::Clear(ClearType::All),style::ResetColor,style::Print("Active showdowns"),cursor::MoveToNextLine(1))?;
        let mut show_map = std::collections::HashMap::new();
        let mut selected_index = 0;
        loop {
            let mut table = table!(["Show","Challenge","Status","Type","Round"]);
            let showdowns = &self.show_repository.get_showdowns().unwrap();
            for s in showdowns {
                show_map.entry(s.show_id.clone()).or_insert_with(|| self.show_repository.get_show(&s.show_id).unwrap());
                table.add_row(row![show_map.get(&s.show_id).unwrap().name,s.title,s.status.to_str().unwrap(),s.type_,format!("{}/{}",s.current_round,s.credit_ids.len()-1)]);
            }
            highlight_row(selected_index, None, &mut table);
            queue!(self.stdout,terminal::Clear(ClearType::All),cursor::MoveTo(0,0))?;
            terminal::disable_raw_mode()?;
            table.print_tty(false);
            terminal::enable_raw_mode()?;
            let showdown = showdowns.get(selected_index).unwrap();
            queue!(self.stdout,cursor::MoveTo(0,terminal::size()?.1),style::Print("[Any Key] to return to List"))?;
            if showdown.is_startable() {
                queue!(self.stdout,cursor::MoveRight(2),style::Print("[S] to start"))?;
            }
            if showdown.is_completable().unwrap() {
                queue!(self.stdout,cursor::MoveRight(2),style::Print("[C] to complete"))?;
            }
            if showdown.is_active() {
                queue!(self.stdout,cursor::MoveRight(2),style::Print("[Enter] To vote on the current round"))?;
                queue!(self.stdout,cursor::MoveRight(2),style::Print("[E] To end current round"))?;
            }
            self.stdout.flush()?;
            match read_char()? {
                KeyCode::Down => {
                    if selected_index < showdowns.len() - 1 {
                        highlight_row(selected_index + 1, Some(selected_index), &mut table);
                        selected_index += 1;
                    }
                }
                KeyCode::Up => {
                    if selected_index > 0 {
                        highlight_row(selected_index - 1, Some(selected_index), &mut table);
                        selected_index -= 1;
                    }
                }
                KeyCode::Char('S') | KeyCode::Char('s') => {
                    if showdown.is_startable() {
                        let mut status = showdown.clone();
                        status.status = ShowdownStatus::Active;
                        status.current_round = 1;
                        self.show_repository.create_showdown(&status).expect("Save started");
                    };
                }
                KeyCode::Char('C') | KeyCode::Char('c') => {
                    if showdown.is_completable().unwrap() {
                        let mut stats = showdown.clone();
                        stats.status = ShowdownStatus::Completed;
                        self.show_repository.create_showdown(&stats).expect("Save completed");
                    };
                }
                KeyCode::Char('E') | KeyCode::Char('e') => {
                    let votes = self.show_repository.get_elimination_votes(&showdown.id, Some(&showdown.current_round)).unwrap();
                    let credit_id: String = votes.into_iter().map(|v| v.credit_id).sorted().dedup_with_count().sorted_by_key(|t| t.0).last().unwrap().1;
                    self.show_repository.create_elimination_round(&showdown.id, &showdown.current_round, &credit_id).unwrap();
                    let mut updated = showdown.clone();
                    updated.current_round += 1;
                    self.show_repository.create_showdown(&updated).unwrap();
                }
                KeyCode::Enter => {
                    if showdown.is_active() {
                        return Ok(VoteOnRound(showdown.clone()));
                    }
                }
                KeyCode::Char('q') | KeyCode::Char('Q') | KeyCode::Esc => {
                    return Ok(Landing);
                }
                _ => {}
            };
        }
    }

    fn show_selection<>(&mut self) -> Result<UserCommand> {
        let mut shows = self.show_repository.get_shows().expect("unable to load shows");
        let (_columns, rows) = terminal::size().unwrap();
        queue!(self.stdout,style::ResetColor,terminal::Clear(ClearType::All),cursor::MoveTo(0,rows-1),style::Print("[Enter] Select a show, [Q] Quit, [N] New Show, [C] Create showdown"))?;
        queue!(self.stdout,cursor::MoveTo(0,0))?;
        let mut table = table!(["Name","Release Year"]);
        for s in &shows {
            table.add_row(row![s.name,s.release_year.as_ref().unwrap_or(&"".to_string())]);
        }
        let mut cursor_offset = 0;
        if !shows.is_empty() {
            highlight_row(cursor_offset, None, &mut table);
        }
        loop {
            queue!(self.stdout,style::ResetColor,cursor::MoveTo(0,0))?;
            terminal::disable_raw_mode()?;
            table.print_tty(false);
            terminal::enable_raw_mode()?;
            self.stdout.flush().unwrap();
            let next_command: Option<UserCommand> = match read_char().expect("Error reading character") {
                KeyCode::Down => {
                    if cursor_offset < shows.len() - 1 {
                        highlight_row(cursor_offset + 1, Some(cursor_offset), &mut table);
                        cursor_offset += 1;
                    }
                    None
                }
                KeyCode::Up => {
                    if cursor_offset > 0 {
                        highlight_row(cursor_offset - 1, Some(cursor_offset), &mut table);
                        cursor_offset -= 1;
                    }
                    None
                }
                KeyCode::Char('q') | KeyCode::Char('Q') => Some(Quit),
                KeyCode::Enter => Some(SelectShow(std::mem::take(&mut shows[cursor_offset]))),
                KeyCode::Char('n') | KeyCode::Char('N') => Some(NewShow),
                KeyCode::Char('c') | KeyCode::Char('C') => Some(CreateShowdown(self.show_repository.get_show(&shows[cursor_offset].tmdb_id).expect("Error getting show"))),
                _ => None
            };
            if next_command.is_some() {
                return Ok(next_command.unwrap());
            }
        }
    }

    fn select_show(&mut self, selected_show: &Show) -> Result<UserCommand> {
        let show = &self.show_repository.get_show(&selected_show.tmdb_id).expect("Error getting show");
        queue!(self.stdout,style::ResetColor,terminal::Clear(ClearType::All),cursor::MoveTo(0,0),style::SetForegroundColor(Color::Magenta),style::Print("Name "))?;
        queue!(self.stdout,style::ResetColor,style::Print(format!("{} ({})",show.name,show.release_year.as_ref().unwrap_or(&String::new()))))?;

        queue!(self.stdout,style::ResetColor,cursor::MoveToNextLine(1),style::SetForegroundColor(Color::Magenta),style::Print("Description "))?;
        queue!(self.stdout,style::ResetColor,style::Print(show.overview.as_ref().unwrap_or(&String::from("")).to_string()))?;

        queue!(self.stdout,style::ResetColor,cursor::MoveToNextLine(1),style::SetForegroundColor(Color::Magenta),style::Print("Characters "),style::ResetColor,cursor::MoveToNextLine(1))?;
        let rows = show.characters.iter().map(|c| row![c.character_name.to_string(),c.actor_name.to_string()]).collect();
        let mut character_table = prettytable::Table::init(rows);
        character_table.set_format(*format::consts::FORMAT_BORDERS_ONLY);
        terminal::disable_raw_mode()?;
        character_table.print_tty(false);
        terminal::enable_raw_mode()?;
        self.stdout.flush()?;
        match read_char()? {
            KeyCode::Char('c') | KeyCode::Char('C') => {
                let clone = &mut show.clone();
                Ok(CreateShowdown(std::mem::take(clone)))
            }
            _ => Ok(ShowSelection)
        }
    }

    fn new_show(&mut self) -> Result<UserCommand> {
        queue!(self.stdout,style::ResetColor,terminal::Clear(ClearType::All),cursor::MoveTo(0,0),
    style::Print("[Show Name]: "))?;
        self.stdout.flush()?;
        let mut show_name = "".to_owned();
        let mut found_shows: Vec<TmdbTvShow> = vec![];
        let mut selected_show_index = 0;
        loop {
            match read_char()? {
                KeyCode::Enter => break,
                KeyCode::Backspace => {
                    if !show_name.is_empty() {
                        show_name.pop();
                        queue!(self.stdout,cursor::MoveTo(0,0),terminal::Clear(ClearType::CurrentLine),style::Print(format!("[Show Name]: {}",&show_name)))?;
                        found_shows = self.tmdb_repository.search_for_show(&show_name).unwrap();
                    }
                }
                KeyCode::Char(c) => {
                    show_name.push(c);
                    queue!(self.stdout,cursor::MoveTo(0,0),terminal::Clear(ClearType::CurrentLine),style::Print(format!("[Show Name]: {}",&show_name)))?;
                    found_shows = self.tmdb_repository.search_for_show(&show_name).unwrap();
                }
                KeyCode::Down => {
                    if selected_show_index < found_shows.len() - 1 {
                        selected_show_index += 1;
                    }
                }
                KeyCode::Up => {
                    if selected_show_index > 0 {
                        selected_show_index -= 1;
                    }
                }
                _ => {}
            };
            queue!(self.stdout,cursor::MoveTo(0,1),terminal::Clear(ClearType::FromCursorDown))?;
            for i in 0..found_shows.len() {
                queue!(self.stdout,cursor::MoveToNextLine(1))?;
                if i == selected_show_index {
                    queue!(self.stdout,style::SetForegroundColor(Color::Cyan),style::Print(found_shows[i].get_formatted_name()),style::ResetColor)?;
                } else {
                    queue!(self.stdout,style::Print(found_shows[i].get_formatted_name()))?;
                }
            }
            self.stdout.flush()?;
        }


        let found_show = &found_shows[selected_show_index];
        queue!(self.stdout,cursor::MoveToNextLine(2),style::Print("Save show? [Y/n] "))?;
        queue!(self.stdout,cursor::MoveToNextLine(2),style::Print(format!("Name: {}",found_show.name.as_ref().unwrap())))?;
        queue!(self.stdout,cursor::MoveToNextLine(1),style::Print(format!("Release Year: {}",found_show.get_release_year().unwrap_or("".to_string()))))?;
        queue!(self.stdout,cursor::MoveToNextLine(1),style::Print(format!("Description: {}",found_show.overview.as_ref().unwrap_or(&"".to_string()))))?;
        self.stdout.flush()?;
        match read_char()? {
            KeyCode::Enter | KeyCode::Char('y') | KeyCode::Char('Y') => {
                let show_data = self.tmdb_repository.get_show(&found_show.id);
                let seasons = show_data.unwrap().seasons.iter().flatten().map(|s| s.name.to_string()).collect();
                let credits: Vec<Character> = self.tmdb_repository.get_credits(&found_show.id).unwrap()
                    .cast.iter().map(Character::from_tmdb_credit).collect();
                let show = Show {
                    name: found_show.name.as_ref().unwrap().to_string(),
                    release_year: found_show.get_release_year().as_ref().map(|s| s.to_string()),
                    overview: found_show.overview.as_ref().map(|s| s.to_string()),
                    tmdb_id: found_show.id,
                    seasons,
                    characters: credits,
                };
                self.show_repository.create_show(&show).expect("Error creating show");
            }
            _ => {}
        };
        Ok(ShowSelection)
    }

    fn create_showdown(&mut self, show: &Show) -> Result<UserCommand> {
        let mut title = "".to_owned();
        execute!(self.stdout,cursor::MoveTo(0,0),terminal::Clear(ClearType::All),style::Print("[Challenge]: "))?;
        loop {
            match read_char()? {
                KeyCode::Enter => break,
                KeyCode::Backspace => {
                    if !title.is_empty() {
                        title.pop();
                        queue!(self.stdout,cursor::MoveTo(0,0),terminal::Clear(ClearType::CurrentLine),style::Print(format!("[Challenge]: {}",&title)))?;
                    }
                }
                KeyCode::Char(c) => {
                    title.push(c);
                    queue!(self.stdout,cursor::MoveTo(0,0),terminal::Clear(ClearType::CurrentLine),style::Print(format!("[Challenge]: {}",&title)))?;
                }
                _ => {}
            };
            self.stdout.flush()?;
        }
        queue!(self.stdout,terminal::Clear(ClearType::All),style::ResetColor,cursor::MoveTo(0,0),style::Print("[1] Entire Show"))?;
        queue!(self.stdout,cursor::MoveToNextLine(1),style::Print("[2] Specific Season"))?;
        queue!(self.stdout,cursor::MoveToNextLine(1),style::Print("[3] Specific Episode"))?;
        self.stdout.flush()?;
        enum SelectionType {
            Show,
            Season,
            Episode,
        }
        let selection_type: SelectionType = loop {
            let s: Option<SelectionType> = match read_char()? {
                KeyCode::Char('1') => Some(SelectionType::Show),
                KeyCode::Char('2') => Some(SelectionType::Season),
                KeyCode::Char('3') => Some(SelectionType::Episode),
                _ => None
            };
            if s.is_some() {
                break s.unwrap();
            }
        };
        let mut showdown = Showdown {
            id: uuid::Uuid::new_v4().to_hyphenated().to_string(),
            show_id: show.tmdb_id,
            season: "".to_string(),
            episode: "".to_string(),
            credit_ids: vec![],
            current_round: 0,
            type_: "ELIMINATION".to_string(),
            title: title.to_string(),
            status: Pending,
        };
        let special_start = &show.seasons[0] == "Specials";
        match selection_type {
            SelectionType::Show => {}
            SelectionType::Season | SelectionType::Episode => {
                let mut selection_offset = if special_start { 1 } else { 0 };

                let mut season_table = table!(["Select Season"]);
                for season in &show.seasons {
                    season_table.add_row(row![season]);
                }
                season_table.set_format(*prettytable::format::consts::FORMAT_CLEAN);
                queue!(self.stdout,cursor::MoveToNextLine(1))?;
                highlight_row(selection_offset, None, &mut season_table);
                let selected_season = loop {
                    queue!(self.stdout,cursor::MoveTo(0,3))?;
                    terminal::disable_raw_mode()?;
                    season_table.print_tty(false);
                    terminal::enable_raw_mode()?;
                    match read_char()? {
                        KeyCode::Up => {
                            if selection_offset > 0 {
                                highlight_row(selection_offset - 1, Some(selection_offset), &mut season_table);
                                selection_offset -= 1;
                            }
                        }
                        KeyCode::Down => {
                            if selection_offset < show.seasons.len() - 1 {
                                highlight_row(selection_offset + 1, Some(selection_offset), &mut season_table);
                                selection_offset += 1;
                            }
                        }
                        KeyCode::Enter => {
                            break &show.seasons[selection_offset];
                        }
                        _ => {}
                    };
                };
                showdown.season = selected_season.clone();
                match selection_type {
                    SelectionType::Episode => {
                        let show1 = self.tmdb_repository.get_show(&show.tmdb_id).unwrap();
                        let season = show1.seasons.iter().flatten().find(|s| s.name == *selected_season).unwrap();
                        let input_message = &format!("Select Episode [1-{}]: ", season.episode_count);
                        queue!(self.stdout,style::ResetColor,cursor::MoveToNextLine(1),style::Print(input_message),cursor::Show)?;
                        self.stdout.flush()?;
                        let mut episode_input = "".to_owned();
                        let episode: String = loop {
                            match read_char()? {
                                KeyCode::Char(c) => {
                                    if c.is_numeric() {
                                        episode_input.push(c);
                                        execute!(self.stdout,style::Print(c))?;
                                    }
                                }
                                KeyCode::Backspace => {
                                    if !episode_input.is_empty() {
                                        episode_input.pop();
                                        execute!(self.stdout,cursor::MoveToColumn((input_message.len()+1).try_into().unwrap()),terminal::Clear(ClearType::UntilNewLine),style::Print(&episode_input))?;
                                    }
                                }
                                KeyCode::Enter => {
                                    execute!(self.stdout,cursor::Hide)?;
                                    break episode_input;
                                }
                                _ => {}
                            };
                        };
                        showdown.episode = episode;
                    }
                    _ => {}
                }
            }
        }
        let characters: Vec<Character> = if !showdown.season.is_empty() && !showdown.episode.is_empty() {
            let season = &show.seasons.iter().position(|r| r == &showdown.season).unwrap() + (if special_start { 0 } else { 1 });
            let credits = self.tmdb_repository.get_episode_credits(&showdown.show_id, &season, &showdown.episode).unwrap();
            let mut characters: Vec<Character> = credits.cast.iter().map(Character::from_tmdb_credit).collect();
            if credits.guest_stars.is_some() {
                credits.guest_stars.unwrap().iter().map(Character::from_tmdb_credit).for_each(|c| characters.push(c));
            }
            self.show_repository.create_characters(&characters, &showdown.show_id, &showdown.season, &showdown.episode).unwrap();
            characters.to_vec()
        } else if !showdown.season.is_empty() {
            let season = &show.seasons.iter().position(|r| r == &showdown.season).unwrap() + (if special_start { 0 } else { 1 });
            let credits = self.tmdb_repository.get_season_credits(&showdown.show_id, &season).unwrap();
            let mut characters: Vec<Character> = credits.cast.iter().map(Character::from_tmdb_credit).collect();
            if credits.guest_stars.is_some() {
                credits.guest_stars.unwrap().iter().map(Character::from_tmdb_credit).for_each(|c| characters.push(c));
            }
            self.show_repository.create_characters(&characters, &showdown.show_id, &showdown.season, &"".to_string()).unwrap();
            characters.to_vec()
        } else {
            show.characters.to_vec()
        };
        queue!(self.stdout,cursor::MoveTo(0,0),terminal::Clear(ClearType::All),style::Print("Select the characters you want to include in the showdown"),cursor::MoveToNextLine(2))?;
        let mut selected_characters = vec![true; characters.len()];
        for character_index in 0..characters.len() {
            if selected_characters[character_index] { queue!(self.stdout,style::SetForegroundColor(Color::Cyan))?; }
            if 0 == character_index { queue!(self.stdout,style::SetBackgroundColor(Color::DarkGrey))?; }
            queue!(self.stdout,style::Print(&characters[character_index].character_name),style::ResetColor,cursor::MoveToNextLine(1))?;
        }
        let (_columns, rows) = terminal::size().unwrap();
        queue!(self.stdout,style::ResetColor,cursor::MoveTo(0,rows-1),style::Print("[Space] Toggle character selection, [Up]/[Down] Change character, [Enter] Proceed"))?;
        queue!(self.stdout,cursor::MoveTo(0,2))?;
        self.stdout.flush()?;
        loop {
            let (_column, r) = cursor::position()?;
            let row = (r - 2) as usize;
            match read_char()? {
                KeyCode::Down => {
                    if row < characters.len() - 1 {
                        if selected_characters[row] { queue!(self.stdout,style::SetForegroundColor(Color::Cyan))?; }
                        queue!(self.stdout,terminal::Clear(ClearType::CurrentLine),cursor::MoveToColumn(0),style::Print(&characters[row].character_name),style::ResetColor,cursor::MoveToNextLine(1))?;
                        if selected_characters[row + 1] { queue!(self.stdout,style::SetForegroundColor(Color::Cyan))?; }
                        queue!(self.stdout,terminal::Clear(ClearType::CurrentLine),cursor::MoveToColumn(0),style::SetBackgroundColor(Color::DarkGrey),style::Print(&characters[row+1].character_name),style::ResetColor)?;
                    }
                }
                KeyCode::Up => {
                    if row > 0 {
                        if selected_characters[row] { queue!(self.stdout,style::SetForegroundColor(Color::Cyan))?; }
                        queue!(self.stdout,terminal::Clear(ClearType::CurrentLine),cursor::MoveToColumn(0),style::Print(&characters[row].character_name),style::ResetColor,cursor::MoveToPreviousLine(1))?;
                        if selected_characters[row - 1] { queue!(self.stdout,style::SetForegroundColor(Color::Cyan))?; }
                        queue!(self.stdout,terminal::Clear(ClearType::CurrentLine),cursor::MoveToColumn(0),style::SetBackgroundColor(Color::DarkGrey),style::Print(&characters[row-1].character_name),style::ResetColor)?;
                    }
                }
                KeyCode::Char(' ') => {
                    selected_characters[row] = !selected_characters[row];
                    if selected_characters[row] { queue!(self.stdout,style::SetForegroundColor(Color::Cyan))?; }
                    queue!(self.stdout,terminal::Clear(ClearType::CurrentLine),cursor::MoveToColumn(0),style::SetBackgroundColor(Color::DarkGrey),style::Print(&characters[row].character_name),style::ResetColor)?;
                }
                KeyCode::Enter => break,
                _ => {}
            };
        }
        let credit_ids: Vec<String> = characters.iter()
            .zip(selected_characters.iter()).filter(|(_c, s)| **s).map(|(c, _s)| c.credit_id.to_string()).collect();
        showdown.credit_ids = credit_ids;
        self.show_repository.create_showdown(&showdown).unwrap();
        Ok(Landing)
    }

    fn vote_on_round(&mut self, showdown: &Showdown) -> Result<UserCommand> {
        if self.user_name.is_none() {
            return Ok(Login(Box::new(Some(VoteOnRound(showdown.clone())))));
        }
        execute!(self.stdout,terminal::Clear(All),cursor::MoveTo(0,0),style::ResetColor,style::Print(&showdown.title),cursor::MoveToNextLine(1),
        style::Print(std::iter::repeat("=").take(terminal::size().unwrap().0.into()).collect::<String>()),cursor::MoveToNextLine(1))?;
        let characters = self.show_repository.get_characters(showdown).unwrap();
        let finished_rounds = self.show_repository.get_elimination_rounds(&showdown.id).unwrap();
        let eliminated_credit_ids: Vec<String> = finished_rounds.into_iter().map(|r| r.credit_id).collect();
        let selectable_credit_ids: Vec<String> = characters.clone().into_iter().filter(|c| !eliminated_credit_ids.contains(&c.credit_id)).map(|c| c.credit_id).collect();
        let user_name = &self.user_name.as_ref().unwrap().clone();
        let existing_vote = self.show_repository.get_elimination_votes(&showdown.id, Some(&showdown.current_round)).unwrap().into_iter().find(|v| v.user_id.eq(user_name));
        let mut selected_credit_id = match existing_vote {
            None => selectable_credit_ids[0].clone(),
            Some(vote) => vote.credit_id.clone()
        };
        let (_columns, rows) = terminal::size()?;

        loop {
            queue!(self.stdout,cursor::MoveTo(0,2),terminal::Clear(ClearType::FromCursorDown))?;
            let mut cursor_line = 2;
            let mut cursor_offset = 0;
            let mut widest_name = 0;
            for character in characters.clone() {
                queue!(self.stdout,cursor::MoveTo(cursor_offset,cursor_line))?;
                if selected_credit_id.eq(&character.credit_id) {
                    queue!(self.stdout,style::SetAttribute(Attribute::Reverse))?;
                } else if eliminated_credit_ids.contains(&character.credit_id) {
                    let mut attributes: Attributes = Attribute::Reset.into();
                    attributes.set(Attribute::Dim);
                    queue!(self.stdout,style::SetAttributes(attributes))?;
                } else {
                    queue!(self.stdout,style::SetAttribute(Attribute::Reset))?;
                };
                queue!(self.stdout,style::Print(&character.character_name))?;
                let name_width = character.character_name.len() as u16;
                if name_width > widest_name {
                    widest_name = name_width;
                }
                cursor_line = if cursor_line == (rows - 1) {
                    cursor_offset += 2 + widest_name;
                    widest_name = 0;
                    2
                } else { cursor_line + 1 };
            }
            let config = Config{
                transparent: false,
                absolute_offset: true,
                x: cursor_offset+widest_name+4,
                y: 2,
                restore_cursor: false,
                width: None,
                height: Some((rows - 4) as u32),
                truecolor: false,
                use_kitty: true,
                use_iterm: false
            };
            let c = characters.clone().into_iter().find(|c|c.credit_id.eq(&selected_credit_id)).unwrap();
            let path = format!(".images/{}", &c.profile_path);
            let image_path = Path::new(&path);
            if !image_path.exists(){
                std::fs::create_dir_all(image_path.parent().unwrap()).unwrap();
                fetch_url(format!("https://image.tmdb.org/t/p/w45{}",&c.profile_path),image_path.to_str().unwrap().to_string())?;
            }
            viuer::print_from_file(image_path,&config).expect("paint image");
            self.stdout.flush()?;
            match read_char()? {
                KeyCode::Down => {
                    let index = selectable_credit_ids.clone().into_iter().position(|c| c.eq(&selected_credit_id)).unwrap();
                    selected_credit_id = if index == selectable_credit_ids.len() - 1 {
                        selectable_credit_ids.first().unwrap().clone()
                    } else {
                        selectable_credit_ids.get(index + 1).unwrap().clone()
                    };
                }
                KeyCode::Up => {
                    let index = selectable_credit_ids.clone().into_iter().position(|c| c.eq(&selected_credit_id)).unwrap();
                    selected_credit_id = if index == 0 {
                        selectable_credit_ids.last().unwrap().clone()
                    } else {
                        selectable_credit_ids.get(index - 1).unwrap().clone()
                    };
                }
                KeyCode::Enter => {
                    self.show_repository.vote_for_elimination(&showdown.id, &showdown.current_round, &selected_credit_id, &self.user_name.as_ref().unwrap()).unwrap();
                    break Ok(Landing);
                }
                _ => {}
            }
        }

    }

    fn login(&mut self, possible_next: &Box<Option<UserCommand>>) -> Result<UserCommand> {
        queue!(self.stdout,style::ResetColor,terminal::Clear(ClearType::All),cursor::MoveTo(0,0),style::Print("[Name]: "))?;
        self.stdout.flush()?;
        let mut user_name = "".to_owned();
        loop {
            match read_char()? {
                KeyCode::Enter => break,
                KeyCode::Backspace => {
                    if !user_name.is_empty() {
                        user_name.pop();
                        queue!(self.stdout,cursor::MoveTo(0,0),terminal::Clear(ClearType::CurrentLine),style::Print(format!("[Name]: {}",&user_name)))?;
                    }
                }
                KeyCode::Char(c) => {
                    user_name.push(c);
                    queue!(self.stdout,cursor::MoveTo(0,0),terminal::Clear(ClearType::CurrentLine),style::Print(format!("[Name]: {}",&user_name)))?;
                }
                _ => {}
            };
            self.stdout.flush()?;
        };
        self.user_name = Some(user_name);
        Ok(possible_next.as_ref().as_ref().unwrap_or(&Landing).clone())
    }

    fn setup_interface(&self) -> Result<()> {
        execute!( &self.stdout,terminal::EnterAlternateScreen)?;
        terminal::enable_raw_mode()?;

        queue!(
            &self.stdout,
            style::ResetColor,
            terminal::Clear(ClearType::All),
            cursor::Hide,
            cursor::MoveTo(1, 1)
        )?;
        Ok(())
    }

    fn teardown_interface(&self) -> Result<()> {
        execute!(
        &self.stdout,
        style::ResetColor,
        terminal::Clear(ClearType::All),
        cursor::Show,
        terminal::LeaveAlternateScreen
    )?;
        terminal::disable_raw_mode()?;
        queue!(&self.stdout,terminal::LeaveAlternateScreen)?;
        Ok(())
    }
}

fn read_char() -> Result<KeyCode> {
    loop {
        if let Ok(Event::Key(KeyEvent { code: c, .. })) = event::read()
        {
            return Ok(c);
        }
    }
}

fn fetch_url(url: String, file_name: String)->Result<()>{
    let response = reqwest::blocking::get(url).unwrap();
    let mut file = std::fs::File::create(file_name)?;
    let mut content =  Cursor::new(response.bytes().unwrap());
    std::io::copy(&mut content, &mut file)?;
    Ok(())
}