use reqwest::blocking::Client;
use reqwest::Error;
use reqwest::header::{AUTHORIZATION, HeaderValue};

#[derive(Debug, Default, serde::Deserialize)]
struct TmdbResponse<T> {
    page: i64,
    total_pages: i64,
    total_results: i64,
    results: Vec<T>,
}

#[derive(Debug, Default, serde::Deserialize)]
pub struct TmdbTvShow {
    pub id: i64,
    pub name: Option<String>,
    pub overview: Option<String>,
    pub first_air_date: Option<String>,
    pub seasons: Option<Vec<TmdbTvSeason>>,
}

#[derive(Debug, Default, serde::Deserialize)]
pub struct TmdbTvSeason {
   pub episode_count: i64,
    pub id: i64,
    pub name: String,
}

#[derive(Debug, Default, serde::Deserialize)]
pub struct TmdbCredits {
    pub cast: Vec<TmdbCredit>,
    pub guest_stars: Option<Vec<TmdbCredit>>
}

#[derive(Debug, Default, serde::Deserialize)]
pub struct TmdbCredit {
    pub id: i64,
    pub credit_id: Option<String>,
    pub name: String,
    pub character: Option<String>,
    pub order: i64,
    pub profile_path: Option<String>,
    pub roles: Option<Vec<TmdbRole>>
}

#[derive(Debug, Default, serde::Deserialize)]
pub struct TmdbRole {
    pub credit_id: String,
    pub character: String
}

impl TmdbTvShow {
    pub fn get_formatted_name(&self) -> String {
        let name = self.name.as_ref().unwrap();
        self.get_release_year()
            .map(|d| format!("{} ({})", name, d))
            .unwrap_or(name.to_string())
    }

    pub fn get_release_year(&self) -> Option<String> {
        self.first_air_date.as_ref()
            .filter(|s| s.len() >= 4)
            .map(|d| d[0..4].to_string())
    }
}

pub struct TmdbRepository {
    tmdb_client: Client,
}

impl TmdbRepository {
    pub fn new() -> TmdbRepository {
        TmdbRepository {
            tmdb_client: get_tmdb_client()
        }
    }

    pub fn search_for_show(&self,query: &String) -> Result<Vec<TmdbTvShow>, Error> {
        let url = "https://api.themoviedb.org/3/search/tv";
        let response = self.tmdb_client.get(url)
            .query(&[("query", query), ("page", &"1".to_string()), ])
            .send().unwrap();
        response.json().map(|x: TmdbResponse<TmdbTvShow>| x.results)
    }

    pub fn get_show(&self,show_id: &i64) -> Result<TmdbTvShow,Error> {
        let response = self.tmdb_client.get(format!("https://api.themoviedb.org/3/tv/{}", show_id))
            .send().unwrap();
        response.json()
    }

    pub fn get_credits(&self,show_id: &i64) -> Result<TmdbCredits,Error> {
        let response = self.tmdb_client.get(format!("https://api.themoviedb.org/3/tv/{}/aggregate_credits", show_id))
            .send().unwrap();
        response.json()
    }

    pub fn get_season_credits(&self,show_id: &i64,season:&usize) -> Result<TmdbCredits,Error> {
        let response = self.tmdb_client.get(format!("https://api.themoviedb.org/3/tv/{}/season/{}/aggregate_credits", show_id,season))
            .send().unwrap();
        response.json()
    }

    pub fn get_episode_credits(&self,show_id: &i64,season:&usize,episode:&String) -> Result<TmdbCredits,Error> {
        let response = self.tmdb_client.get(format!("https://api.themoviedb.org/3/tv/{}/season/{}/episode/{}/credits", show_id,season,episode))
            .send().unwrap();
        response.json()
    }
}

fn get_tmdb_client() -> Client {
    let token = std::env::var("TMDB_TOKEN").unwrap();
    let headers = std::iter::once((AUTHORIZATION, HeaderValue::try_from(format!("Bearer {}",token)).unwrap())).collect();
    Client::builder()
        .default_headers(headers)
        .build().unwrap()
}
